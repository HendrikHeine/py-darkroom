import pyinsane2
from pyinsane2.sane.abstract import Image

pyinsane2.init()
try:
	devices = pyinsane2.get_devices()
	assert(len(devices) > 0)
	device = devices[0]
	print("I'm going to use the following scanner: %s" % (str(device)))

	pyinsane2.set_scanner_opt(device, 'resolution', [1200])
	pyinsane2.set_scanner_opt(device, 'mode', ['Color'])
	pyinsane2.maximize_scan_area(device)

	scan_session = device.scan(multiple=False)
	try:
		while True:
			scan_session.scan.read()
	except EOFError:
		pass
	image = scan_session.images[-1]
	image:Image.Image
	image.save("image.jpg", "JPEG")
finally:
	pyinsane2.exit()
